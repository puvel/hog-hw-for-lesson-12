using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FindComponentAbstract : MonoBehaviour
{
    // there may be some components of the method below
    public virtual void DoEffect()
    {
        Debug.Log("================================================================");
        Debug.Log("You found the item!");

    }
}
