using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    private void Awake()
    {
        Initialize();                      
    }

    private void Start()
    {

    }


    [SerializeField] private List<GameItem> _gameItems = new List<GameItem>();
    private int _gameItemsCount;
    public void Initialize()
    {
        //GameItem[] gameItems = GetComponentsInChildren<GameItem>();

        //_gameItems.AddRange(gameItems);
        _gameItemsCount = _gameItems.Count;

        for (int i = 0; i < _gameItems.Count; i++)
        {
            _gameItems[i].OnFind += OnItemFound;   
        }
    }

    private void OnItemFound()
    {
        _gameItemsCount = _gameItemsCount -1;

        if (_gameItemsCount == 0)
        {
            Debug.Log("Level completed, game items counter = 0");
        }
    }
}
