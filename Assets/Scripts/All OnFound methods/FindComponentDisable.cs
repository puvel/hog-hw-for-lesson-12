using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FindComponentDisable : FindComponentAbstract
{
    private SpriteRenderer _spriteRenderer;

    [SerializeField] private float _fadeDuration;


    public void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();   // ��� ����� ������ �������� ������ ��� ����, � ��
    }


    public override void DoEffect()
    {        
        base.DoEffect();
        _spriteRenderer.DOFade(0f, _fadeDuration).OnComplete( () =>
        {
            gameObject.SetActive(false);
        });
        Debug.Log("That was a disabled mehtod");
    }


}
