using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FindComponentRotation : FindComponentAbstract
{
    [SerializeField] private float _rotateDuration;
    private Rigidbody2D _rigidBody;

    private void awake()
    {
        _rigidBody = gameObject.GetComponent<Rigidbody2D>();
    }



    public override void DoEffect()    
    {
        base.DoEffect();

        transform.DORotate(new Vector3(360, 360, 0),  _rotateDuration, RotateMode.FastBeyond360).OnComplete(() =>  // ����� ����� ������
        {
            gameObject.SetActive(false);
        });

        Debug.Log("That was a rotation method");
    }
}
