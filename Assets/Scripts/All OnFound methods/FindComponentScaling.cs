using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FindComponentScaling : FindComponentAbstract
{
    [SerializeField] private float _newScale;
    [SerializeField] private float _scaleDuration;

   
    public override void DoEffect()
    {
        base.DoEffect();
        transform.DOScale(_newScale, _scaleDuration).OnComplete(() =>  //����������� ������ �����
        {
            gameObject.SetActive(false);
        });
        Debug.Log("That was a scaling method");
        // � ��� ������ ������� � � �
        //��� ������� 29, �� ��� �� ������ ������ ���������� ������
        //private void disableObject()
        //{
        //    gameObject.SetActive(false);
        //}
    }
}
