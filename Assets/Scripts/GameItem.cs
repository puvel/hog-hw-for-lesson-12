using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening; // ��� ����� ��� ����, ����� ������ ���-�� � ����� ���������,    ������

[RequireComponent(typeof(SpriteRenderer))]


public class GameItem : MonoBehaviour
{
    public event Action OnFind;


    private SpriteRenderer _spriteRenderer;

    private FindComponentAbstract _onFindComponent;    


    private void Awake()        
    {
        _onFindComponent = GetComponent<FindComponentAbstract>();
    }
    private void OnMouseUpAsButton()
    {
        // if (SpriteRenderer != null) ��� ��� �� ����, �.� �� ������������ �� ����� [] ����)) require component

        _onFindComponent.DoEffect();
        OnFind?.Invoke();          // ������, � ������ �� ����� ������������ ���� OnFInd, ���� ����� �������� gameObject � DoEffect ??
        OnFind -= null;
    }
}
